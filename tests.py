from pywinauto.application import Application
from pywinauto.findbestmatch import MatchError
from time import sleep
from os import path, remove

import unittest
import sys


class NotepadApplicationTests(unittest.TestCase):
	temporary_file_name = "example.txt"
	
	def __init__(self, *args, **kwargs):
		super(NotepadApplicationTests, self).__init__(*args, **kwargs)
		self.app = None
		self.dlg = None
		self.process_id = None
		
	def setUp(self):
		"""
		Open application
		"""
		print "\nExecuting ", self._testMethodName
		self.app = Application(backend="win32").start('notepad.exe')
		self.dlg = self.app.UntitledNotepad
		self.process_id = self.dlg.process_id()
		actionable_dlg = self.dlg.wait('visible')
		
	def test_about_notepad(self):
		"""
		Display basic information about this version of Notepad.
		"""
		self.app.UntitledNotepad.menu_select("Help -> About Notepad")
		sleep(2)
		self.app.AboutNotepad.OK.Click()
		 
	def test_write_text_without_saving(self):
		"""
		Close application without a content saving.
		"""
		self.app.UntitledNotepad.Edit.set_edit_text("My text")
		# self.assertEqual("My text", self.app.UntitledNotepad.Edit.get_edit_text())
		self.app.UntitledNotepad.menu_select("File -> Exit")
		self.app.Notepad.DontSave.Click()
		
	def test_write_text_with_saving(self):
		"""
		Close application with a content saving.
		"""
		self.app.UntitledNotepad.Edit.set_edit_text("My text")
		# self.assertEqual("My text", self.app.UntitledNotepad.Edit.get_edit_text())
		self.app.UntitledNotepad.menu_select("File -> SaveAs")
		self.app.SaveAs.Edit.type_keys("D:{ENTER}", with_spaces = True)
		self.app.SaveAs.edit1.SetText(self.temporary_file_name)
		self.app.SaveAs.Save.Click()
		
		try:
			self.app.ConfirmSaveAs.Yes.wait('exists').close_click()
		except:
			print 'Skip overwriting...'
		
	def tearDown(self):
		"""
		Close application
		"""
		try:
			self.app.UntitledNotepad.menu_select("File -> Exit")
		except:
			print 'Application has been closed'
		
		if path.isfile("D:" + self.temporary_file_name):
			remove("D:" + self.temporary_file_name)


if __name__ == '__main__':
    unittest.main()
